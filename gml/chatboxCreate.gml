/// ONLINE
@paddingText = 10;
@width = 250;
@padding = 15;
@alpha = 1;
@fade = false;
@fadeAlpha = 1;
@t = 70;
@sep = -1;
@maxTextWidth = @width-2*@paddingText;
@hasDestroyed = false;
